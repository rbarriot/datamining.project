Ce répertoire devrait contenir les fichiers de données de départ accompagnés de leur description (et l'indication de la provenance).

Si les données sont trop volumineuses, alors il faudra indiquer dans ce README.md la procédure pour les récupérer.